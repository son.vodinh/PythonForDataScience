import  string
import random

def input_get_odd_even_array(typeFlat):
    #int an array 
    a=[]
    print("Please input a serial int number:" )
    a=[int(x) for x in input().split(',')]
    odd_array=[]
    even_array=[]
    for i in a:
        if i % 2 :
            odd_array.append(i)
        else:
            even_array.append(i)
    if typeFlat==True:
        return odd_array
    else:
        return even_array

def sort_sum (arrayInput):
    sum=0
    for j in arrayInput:
        if (type(j) is int) or (type(j) is float):
            sum=sum+j
        if type(j) is list:
            for t in j :
                if type(float(t) is float):
                    sum=sum+float(t)
    return sum

def print_array(a):
    for i in a:
        print( i)


def main():
    #exercise 1
    """
    Write a python script that gets a string of number from user, e.g., 12, 32, 31, 12, 42, 21, 58, 92,37,45
    Using this string to generate 2 list of numbers, the first list consists of even numbers and the second one is the list of odd numbers.
    """
    
    #get odd array
    #a1=input_get_odd_even_array(True)
    #print_array(a1)
    #get even array
    #a2=input_get_odd_even_array(False)
    #print_array(a2)
    """
    Exercise :2
    Given the following list l=[(1,4,'ewe','5'), ('21', 0.4, 4, [31,3,5]), [7,3,'s',2], [4,2,6,'dad'], {3,5}]], sort this list by sum of its item which has type is int or float (no recursive).
    OUTPUT:  [('21', 0.4, 4, [31, 3, 5]),  (1, 4, 'ewe', '5'),  {3, 5},  [7, 3, 's', 2],  [4, 2, 6, 'dad']]
    GUIDE: check type(x) is int or float
    """
    #l=[(1,4,'ewe','5'), ('21', 0.4, 4, [31,3,5]), [7,3,'s',2], [4,2,6,'dad'], {3,5}]
    #l.sort(key=sort_sum, reverse=True)
    #print (l)
    """
    Ex:3
    Given a string s = 'the output that tells you if the object is the type you think or not', 
    sort the words of this string by alphabet.
    OUTPUT: 'if is not object or output tells that the the the think type you you'
    upper case all the first character of the words
    OUTPUT: 'The Output That Tells You If The Object Is The Type You Think Or Not'
    GUIDE: both use split and join of string
    """
    s = 'the output that tells you if the object is the type you think or not'
    # breakdown the string into a list of words
    # Using the split() method the string is converted into a list of words.
    #  The split() method splits the string at whitespaces.
    list_words=s.split()
    #sort the list
    list_words.sort()

    s1=""
    for word in list_words:
        s1+=str(word)+" "
    #omit the last space
    s1=s1[:-1]
    print(s1)
    #============================================================#
    s = 'the output that tells you if the object is the type you think or not'
    #use lamda condition
    lst=[word[0].upper()+word[1:] for word in s.split()]
    s2=" ".join(lst)
    print (s2)
    #clear code way
    words=s.split()
    captital_word=[]
    for word in words:
        upcase_word=word[:1].upper()+word[1:]
        captital_word.append(upcase_word)
    print(' '.join(captital_word))
    #Ex 04:
    """
    Write a python script to randomly generate a list of N integers on interval [0,10] (N is a number given by user) and then: 
    Create and print a histogram of the list (i.e., count frequency of occurrences).
    Manually calculate and print the statistics description of the list (i.e, max, min, mean, variance and standard deviation). You may need to import math library and use function sqrt for this calculation.
    Remove duplicates from the list
    """
    print("Input a number:")
    input_number=input()
    print(input_number)
    lst_random=[]
    int_number=0
    while int_number<int(input_number):
        lst_random.append(random.randint(0,10))
        int_number=int_number+1

    print(lst_random)
    #count frequency
    for item in lst_random:
        print ("Frequency of item:"+str(item)+" is "+str(lst_random.count(item)))
    print ("Max value:"+str(max(lst_random)))
    print("Miv value:" + str(min(lst_random)))
    #mean= average
    m=sum(lst_random)/float(len(lst_random))
    print ("Average number is :"+str(m))
    #Ex05================================================================
    """"
    Write a python script to generate a list of 100 numbers of float using 
    the function y = x ^ 2 / (4*x), x on interval (0, 10] and linear space.
    """

    list100=[]
    count=0
    while count<100:
        x = random.randint(0, 10)
        if (x == 0):
            pass
        else:
            y=(x^2)/(4*x)
            list100.append(y)
            count=count+1

    print (list100)
    #Ex 06
    """
    Write a python script to get sum of column of matrix N x M (i.e, list in a list). For example: 
    M = [[1,2,3,4,5],
        [3,4,2,5,6],
        [1,6,3,2,5]]
        OUTPUT: [5, 12, 8, 11, 16]

    """
    list_n_m=[[1,2,3,4,5],[3,4,2,5,6],[1,6,3,2,5]]
    list_count_column=[]
    rows=0
    cols=0
    while cols<5:
        sum1=list_n_m[rows][cols]+list_n_m[rows+1][cols]+list_n_m[rows+2][cols]
        list_count_column.append(sum1)
        cols = cols + 1
    print (list_count_column)





if __name__== "__main__":
  main()