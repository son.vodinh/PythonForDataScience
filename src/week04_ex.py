import pandas as pd
import pandocfilters as doc_filter
"""
    -Given the following string
    -split it into list of words and then use this list to create a Series of word.
    -Filter to extract all words that contains at least 3 vowels (i.e., ‘a’,’i’,’o’,’e’,’u’) of the Series.

    
"""
def count_vowels(word):
    vowels = 'aeiou'
    count=0
    for vowel in vowels:
        if vowel in word:
            count=count+1
    return count


def split_word(text):
    #slipt the text
    words=text.split()
    #print word
    new_lst=[]
    for word in words:
        if count_vowels(word.lower())>2:
            new_lst.append(word)

    return new_lst

def call_ex01():
    str_text="Ah meta descriptions… the last bastion of traditional marketing! The only cross-over point between marketing and search engine optimisation! The knife edge between beautiful branding and an online suicide note!"
    #slipt string to list of word
    lst_words=split_word(str_text)
    #create series of word
    s1=pd.Series(lst_words)
    #print (s1)
    #Filter to extract all words that contains at least 3 vowel
   #s1[s1.filter(regex='(‘a’,’i’,’o’,’e’,’u’)')]
    print (s1)


def main():
    call_ex01()
    # call_ex03()
    # call_ex04()
    #call_ex05()


if __name__ == "__main__":
    main()