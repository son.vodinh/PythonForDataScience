import datetime
#Function  print current date
def print_date():
    #get current time and date
    now = datetime.datetime.now()
    #print
    print ("Current date and time : ")
    print (now.strftime("%Y-%m-%d %H:%M:%S"))
#sum 2 number
def sum(x, y):
    sum = x + y
    # if  15<sum<20
    if sum in range(15, 20):
        return 20
    else:
        return sum

def main():
    #print date
    print_date()
    #print value sum
    print(sum(10, 6))
    print(sum(10, 2))
    print(sum(10, 12))


if __name__ == "__main__":
    main()