import numpy as np

#find its closest values in the array
def find_nearest(array, value):
    array = np.asarray(array)
    "Element in np array `array` closest to the scalar value `value`"
    idx = (np.abs(array - value)).argmin()
    return array[idx]

"""
Exercise 2:
 Write a python script to get a number N from user then create 3D array N x N x N with random (float) values on interval [0,10].
    o Find min, max and sum of array by 3 axis (i.e., deep, height and width).
"""

def call_ex02():
    print("Input a number :")
    inputN=int(input())
    print(inputN)
    #  Randomly constructing 3D array
    # Python Program illustrating
    # numpy.random.rand() method
    #arr=np.random.rand(inputN,inputN,inputN)
    #print(arr)
    #way 2
    arr1=[]
    i=0 
    # generate list  use np.random.uniform(low=0.5, high=13.3, size=(50,))
    arr1=np.random.uniform(low=0,high=10,size=(inputN,inputN,inputN)) #size=inputN*inputN*inputN)
    #reshape it to 3D array
    #arr1=arr1.reshape(inputN,inputN,inputN)
    print("array1 for way 2:",arr1)
    
    #Find min /max
    maxValue=np.max(arr1)
    print("Max value:",maxValue)
    minValue=np.min(arr1)
    print("Min value:",minValue)
    
    #sum of array by 3 axis (i.e., deep, height and width).
    sumAxis0=np.sum(arr1,axis=0)
    print ("Sum axis 0:",sumAxis0)
    sumAxis1=np.sum(arr1,axis=1)
    print ("Sum axis 1:",sumAxis1)
    sumAxis2=np.sum(arr1,axis=2)
    print ("Sum axis 2:",sumAxis2)
    
    
"""
Ex03:  Write a python script to get three number M, N and K from user then create 3D array M x N x K with random integer 
     numbers on interval [-100,100].
o     Find min, max and sum of array by 3 axis (i.e., deep, height and width).

"""
def call_ex03():
    print("Input m  :")
    m=int(input())
    print("Input n  :")
    n=int(input())
    print("Input k  :")
    k=int(input())
    # generate list  use np.random.randint(low=value, high=value, size=(axis0, axis1,axis2))
    arr=np.random.randint(low=-100,high=100,size=(m,n,k))
    print ("Array 3 D of int:")
    print(arr)
    #Find min /max
    maxValue=np.max(arr)
    print("Max value:\n",maxValue)
    minValue=np.min(arr)
    print("Min value:\n",minValue)
    
    #sum of array by 3 axis (i.e., deep, height and width).
    sumAxis0=np.sum(arr,axis=0)
    print ("Sum axis 0:",sumAxis0)
    sumAxis1=np.sum(arr,axis=1)
    print ("Sum axis 1:",sumAxis1)
    sumAxis2=np.sum(arr,axis=2)
    print ("Sum axis 2:",sumAxis2)
"""
Ex04 : Write a python script to randomly generate a 2D array 10 x 8 of float values on interval [-10, 10].
o    And then, get a float number from user and find its closest values in the array. SOME GUIDES: 
i.    i) use argmin() function for your problem: https://docs.scipy.org/doc/numpy/reference/generated/numpy.argmin.html;
ii.    Or reshape to 1D, subtract array to value and sort. 
o    Optional: If you passed the above problem, extend it to find 3 closest values in the array.


"""
def call_ex04():
    
    # generate list float  use np.random.uniform(low=value, high=value, size=(axis0, axis1,axis2))
    #arr=np.random.uniform(low=-10,high=10,size=(10,8))
    arr=np.random.randint(low=0,high=10,size=(3,2))
    print ("Array 2D 10X8:")
    print(arr)
    #Input a float value and find the closest on
    #hint:  use argmin() function for your problem
    """
    Returns the indices of the minimum values along an axis.
        Parameters:    
        a : array_like
        Input array.
        axis : int, optional
        By default, the index is into the flattened array, otherwise along the specified axis.
        out : array, optional
        If provided, the result will be inserted into this array. It should be of the appropriate shape and dtype.
        Returns:    
        index_array : ndarray of ints
        Array of indices into the array. It has the same shape as a.shape with the dimension along axis removed.
    """
    #convert to 1D array:
    arr1=sorted(arr.reshape(-1))
    
    print(arr1)
    # And then, get a float number from user and find its closest values in the array
    print("input a float values:")
    i=float(input())
    #arr1=arr1.astype('float64')
    int_index=find_nearest(arr1,i)
    print("The nearest number is :",int_index)
    #Optional: If you passed the above problem, extend it to find 3 closest values in the array.
    """
    A simple solution is to do linear search for k closest elements.
        need to sorted the array before to do 
        1) Start from the first element and search for the crossover point 
        (The point before which elements are smaller than or equal to X and after which elements are greater). 
        This step takes O(n) time.
        2) Once we find the crossover point, we can compare elements on both sides of crossover point to 
        print k closest elements. This step takes O(k) time.
    """
    
""" Financial functions====================
"""
"""
numpy.fv(rate, nper, pmt, pv, when='end')[source])
Compute the future value.
Parameters:    
rate : scalar or array_like of shape(M, )
Rate of interest as decimal (not per cent) per period

nper : scalar or array_like of shape(M, )
Number of compounding periods

pmt : scalar or array_like of shape(M, )
Payment

pv : scalar or array_like of shape(M, )
Present value

when : 
When payments are due 

Returns:    
out : ndarray
Future values. If all input is scalar, returns a scalar float. If any input is array_like, returns future values for each input element. If multiple inputs are array_like, they all must have the same shape.
"""
"""
Examples

What is the future value after 10 years of saving $100 now, with an additional monthly savings of $100. 
Assume the interest rate is 5% (annually) compounded monthly?
numpy.fv(rate, nper, pmt, pv, when='end')[source])
rate=0.05/12  =>  a = np.array((0.05, 0.06, 0.07))/12 --lai suat 3 loai >>> np.fv(a, 10*12, -100, -100) - gia tri sau 10 nam cua 3 loai 
nper=10*12 (10 nam*12 thang)
pmt=-100
pv=-100
"""
def fv_ex():
    return (np.fv(0.05/12,120,-100,-100))
#pv(rate, nper, pmt[, fv, when])
#Compute the present value.
"""
rate : array_like
Rate of interest (per period)

nper : array_like
Number of compounding periods

pmt : array_like
Payment

fv : array_like, optional
Future value

when :
When payments are due 

Returns:    
out : ndarray, float
Present value of a series of payments or investments.
"""
"""
What is the present value (e.g., the initial investment) of an investment that needs to total $15692.93 after
10 years of saving $100 every month?
Assume the interest rate is 5% (annually) compounded monthly.
pv(rate, nper, pmt[, fv, when])
rate=0.05/12
nper=10*12
pmt=-100
fv=$15692.93
"""
def  pv_ex():  
    return (np.pv(0.05/12,10*12,-100,15692.93))

def call_ex05():
    #Compute the future value.
    fl_future_value=fv_ex()
    print ("the future value after 10 years of saving $100 monthly is : ",fl_future_value)
    #Compute the present value.
    fl_present_value= pv_ex()
    print ("the current value of an investment that needs to total $15692.93 after 10 years of saving $100 every month : "
           ,fl_present_value)
    #Returns the NPV (Net Present Value) of a cash flow series -npv(rate, values)
        #rate : scalar -The discount rate.
        #values:The values of the time series of cash flows. 
            #The fixed time interval between cash flow  events must be the same as that for which rate is given 
            # i.e., if rate is per year, then precisely a year is understood to elapse between each cash flow event 
            #By convention, investments or deposits  are negative, income or  withdrawals are positive;
            # values must begin with the initial investment, thus values[0] will typically be negative.
    print("Net Present Value is:",np.npv(0.28,[-100,20,60,83,86]))
    
def main():
    #call_ex02()
    #call_ex03()
    #call_ex04()
    call_ex05()
        

    
 
    

if __name__== "__main__":
  main()